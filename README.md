# databricks-wyseday

## Prerequisites

In the [prerequisites](prerequisites) folder, you will find a documentation on what to install to participate to the
training.

## Practicals

[tp0-databricks-hands-on](tp0-databricks-hands-on) 

[tp1-notebook](tp1-notebook)

[tp2-workflow](tp2-workflow)

[tp3-sql-editor](tp3-sql-editor) 

[tp4-dashboard](tp4-dashboard)